import React from 'react'
import { useNavigate } from 'react-router-dom'

const ErrorPage = () => {
  const navigate = useNavigate()
  return (
    <div className='text-center text-danger'>
        <h1>
            ERROR PAGE NOT FOUND!
        </h1>
        <button className='btn btn-secondary mt-3' onClick={() => navigate(-1)}>Back to previous page</button>
    </div>
  )
}

export default ErrorPage