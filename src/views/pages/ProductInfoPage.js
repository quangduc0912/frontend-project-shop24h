import React from 'react'
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import BreadcrumbsComponent from '../../components/Breadcrumbs/BreadcrumbsComponent';
import ProductInfoComponent from '../../components/Content/Products/ProductInfoComponent';

const ProductInfoPage = () => {
  const routes = [
    {
      name: "Home",
      path: "/"
    },
    {
      name: "All Products",
      path: "/products",
    },
    {
      name: "Product Details",
      path: "/products/:productId"
    }
  ]
  return (
    <>
        <Header/>
        <div className='container justify-content-center' >
          <BreadcrumbsComponent routes={routes}/>
          <ProductInfoComponent/>
        </div>
        <Footer/>
    </>
  )
}

export default ProductInfoPage