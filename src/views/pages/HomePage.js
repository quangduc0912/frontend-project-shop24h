import React from 'react'
import Content from '../../components/Content/Content'
import Footer from '../../components/Footer/Footer'
import Header from '../../components/Header/Header'
import Slide from '../../components/Content/Slider/Slider'

const HomePage = () => {
  return (
    <>
        <Header/>
        <Slide/>
        <Content/>
        <Footer/>
    </>
  )
}

export default HomePage