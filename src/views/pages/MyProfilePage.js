import React from 'react'
import BreadcrumbsComponent from '../../components/Breadcrumbs/BreadcrumbsComponent'
import MySpaceComponent from '../../components/MySpace/MySpace'
import Footer from '../../components/Footer/Footer'
import Header from '../../components/Header/Header'

const MyProfilePage = () => {
  const routes = [
    {
      name: "Home",
      path: "/"
    },
    {
      name: "My Space",
      path: "/myspace"
    }
  ]
  return (
    <>
        <Header/>
        <div className="container justify-content-center">
        <BreadcrumbsComponent routes={routes}/>
        <MySpaceComponent/>
        </div>
        <Footer/>
    </>
  )
}

export default MyProfilePage