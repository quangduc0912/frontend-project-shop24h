import React from 'react'
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import BreadcrumbsComponent from '../../components/Breadcrumbs/BreadcrumbsComponent';
import AllProductsComponent from '../../components/Content/Products/AllProductsComponent';

const ProductPage = () => {
  const routes = [
    {
      name: "Home",
      path: "/"
    },
    {
      name: "All Products",
      path: "/products"
    }
  ]
 
  return (
    <>
        <Header/>
        <div className='container justify-content-center' >
          <BreadcrumbsComponent routes={routes}/>
          <AllProductsComponent/>
        </div>
        <Footer/>
    </>
  )
}

export default ProductPage