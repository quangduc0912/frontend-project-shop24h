import React from "react";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import BreadcrumbsComponent from "../../components/Breadcrumbs/BreadcrumbsComponent";
import ShoppingCartComponent from "../../components/ShoppingCart/ShoppingCart";


const CartPage = () => {
  const routes = [
    {
      name: "Home",
      path: "/"
    },
    {
      name: "Your Cart",
      path: "/cart"
    }
  ]
  return (
    <>
      <Header />
      <div className="container justify-content-center">
        <BreadcrumbsComponent routes={routes}/>
        <ShoppingCartComponent/>
      </div>
      <Footer />
    </>
  );
};

export default CartPage;
