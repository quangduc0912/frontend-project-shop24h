import React from "react";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import BreadcrumbsComponent from "../../components/Breadcrumbs/BreadcrumbsComponent";
import OrderSuccessComponent from "../../components/Order/OrderSuccess";


const OrderSuccessPage = () => {
  const routes = [
    {
      name: "Home",
      path: "/"
    },
    {
      name: "Your Order",
      path: "/order"
    }
  ]
  return (
    <>
      <Header />
      <div className="container justify-content-center">
        <BreadcrumbsComponent routes={routes} />
        <OrderSuccessComponent/>
      </div>
      <Footer />
    </>
  );
};

export default OrderSuccessPage;
