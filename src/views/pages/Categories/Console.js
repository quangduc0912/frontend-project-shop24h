import React from "react";
import Header from "../../../components/Header/Header";
import BreadcrumbsComponent from "../../../components/Breadcrumbs/BreadcrumbsComponent";
import Footer from "../../../components/Footer/Footer";
import ConsoleComponent from "../../../components/Content/Categories/ConsoleComponent ";

const Console = () => {
  const routes = [
    {
      name: "Home",
      path: "/"
    },
    {
      name: "All Consoles",
      path: "/console"
    }
  ]
  return (
    <>
      <Header />
      <div className="container justify-content-center">
        <BreadcrumbsComponent routes={routes} />
        <ConsoleComponent />
      </div>
      <Footer />
    </>
  );
};

export default Console;
