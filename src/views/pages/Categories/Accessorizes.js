import React from "react";
import Header from "../../../components/Header/Header";
import BreadcrumbsComponent from "../../../components/Breadcrumbs/BreadcrumbsComponent";
import Footer from "../../../components/Footer/Footer";
import AccessorizesComponent from "../../../components/Content/Categories/AccessorizesComponent";

const Accessorizes = () => {
  const routes = [
    {
      name: "Home",
      path: "/"
    },
    {
      name: "Accessorizes",
      path: "/accessorizes"
    }
  ]
  return (
    <>
      <Header />
      <div className="container justify-content-center">
        <BreadcrumbsComponent routes={routes} />
        <AccessorizesComponent />
      </div>
      <Footer />
    </>
  );
};

export default Accessorizes;
