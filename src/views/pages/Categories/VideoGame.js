import React from "react";
import Header from "../../../components/Header/Header";
import BreadcrumbsComponent from "../../../components/Breadcrumbs/BreadcrumbsComponent";
import Footer from "../../../components/Footer/Footer";
import VideoGameComponent from "../../../components/Content/Categories/VideoGameComponent";

const VideoGame = () => {
  const routes = [
    {
      name: "Home",
      path: "/"
    },
    {
      name: "Video Games",
      path: "/videogames"
    }
  ]
  return (
    <>
      <Header />
      <div className="container justify-content-center">
        <BreadcrumbsComponent routes={routes}/>
        <VideoGameComponent />
      </div>
      <Footer />
    </>
  );
};

export default VideoGame;
