import React from 'react'
import BreadcrumbsComponent from '../../components/Breadcrumbs/BreadcrumbsComponent'
import SignUpComponent from '../../components/SignUp/SignUp'
import Footer from '../../components/Footer/Footer'
import Header from '../../components/Header/Header'

const SignUpPage = () => {
  const routes = [
    {
      name: "Home",
      path: "/"
    },
    {
      name: "Sign Up",
      path: "/signup"
    }
  ]
  return (
    <>
        <Header/>
        <div className="container justify-content-center">
        <BreadcrumbsComponent routes={routes}/>
        <SignUpComponent/>
        </div>
        <Footer/>
    </>
  )
}

export default SignUpPage