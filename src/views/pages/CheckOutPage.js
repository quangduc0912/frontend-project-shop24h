import React from 'react'
import BreadcrumbsComponent from '../../components/Breadcrumbs/BreadcrumbsComponent'
import CheckOutComponent from '../../components/Content/CheckOut/CheckOut'
import Footer from '../../components/Footer/Footer'
import Header from '../../components/Header/Header'

const CheckOutPage = () => {
  const routes = [
    {
      name: "Home",
      path: "/"
    },
    {
      name: "Your Cart",
      path: "/cart"
    },
    {
      name: "Check Out",
      path: "/cart/checkout"
    }
  ]
  return (
    <>
        <Header/>
        <div className="container justify-content-center">
        <BreadcrumbsComponent routes={routes}/>
        <CheckOutComponent/>
        </div>
        <Footer/>
    </>
  )
}

export default CheckOutPage