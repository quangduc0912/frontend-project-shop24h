import React from 'react'
import Header from '../../../components/Header/Header'
import Footer from '../../../components/Footer/Footer'
import PCGamingComponent from '../../../components/Content/Platform/PC'
import BreadcrumbsComponent from '../../../components/Breadcrumbs/BreadcrumbsComponent'

const PcPage = () => {
    const routes = [
        {
          name: "Home",
          path: "/"
        },
        {
          name: "PC Gaming Platform",
          path: "/pc"
        }
      ]
    return (
        <>
            <Header/>
            <div className="container justify-content-center">
            <BreadcrumbsComponent routes={routes}/>
            <PCGamingComponent/>
            </div>
            <Footer/>
        </>
      )
}

export default PcPage
