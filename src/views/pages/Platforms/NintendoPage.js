import React from 'react'
import Header from '../../../components/Header/Header'
import Footer from '../../../components/Footer/Footer'
import NintendoComponent from '../../../components/Content/Platform/Nintendo'
import BreadcrumbsComponent from '../../../components/Breadcrumbs/BreadcrumbsComponent'

const NintendoPage = () => {
    const routes = [
        {
          name: "Home",
          path: "/"
        },
        {
          name: "Nintendo Platform",
          path: "/nintendo"
        }
      ]
    return (
        <>
            <Header/>
            <div className="container justify-content-center">
            <BreadcrumbsComponent routes={routes}/>
            <NintendoComponent/>
            </div>
            <Footer/>
        </>
      )
}

export default NintendoPage
