import React from 'react'
import Header from '../../../components/Header/Header'
import Footer from '../../../components/Footer/Footer'
import XboxComponent from '../../../components/Content/Platform/Xbox'
import BreadcrumbsComponent from '../../../components/Breadcrumbs/BreadcrumbsComponent'

const XboxPage = () => {
    const routes = [
        {
          name: "Home",
          path: "/"
        },
        {
          name: "Xbox Platform",
          path: "/xbox"
        }
      ]
    return (
        <>
            <Header/>
            <div className="container justify-content-center">
            <BreadcrumbsComponent routes={routes}/>
            <XboxComponent/>
            </div>
            <Footer/>
        </>
      )
}

export default XboxPage
