import React from 'react'
import Header from '../../../components/Header/Header'
import Footer from '../../../components/Footer/Footer'
import PlaystationComponent from '../../../components/Content/Platform/Playstation'
import BreadcrumbsComponent from '../../../components/Breadcrumbs/BreadcrumbsComponent'

const PlaystationPage = () => {
    const routes = [
        {
          name: "Home",
          path: "/"
        },
        {
          name: "Playstation Platform",
          path: "/playstation"
        }
      ]
    return (
        <>
            <Header/>
            <div className="container justify-content-center">
            <BreadcrumbsComponent routes={routes}/>
            <PlaystationComponent/>
            </div>
            <Footer/>
        </>
      )
}

export default PlaystationPage
