import React, { useEffect, useState } from "react";
import { Container, Col, Table } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { ThumbUp } from "@mui/icons-material";
import { TextField } from "@mui/material";
import { toast } from "react-toastify";
import { loggedInUser } from "../../app/redux/userSlice";

const MySpaceComponent = () => {
  const user = useSelector((state) => state.user.user);
  console.log(user)
  const [updateUser, setUpdateUser] = useState({
    fullName: user.fullName || user.displayName,
    email: user.email,
    phone: user.phoneNumber || user.phone,
    address: user.address,
    city: user.city,
    country: user.country,
  });
  const dispatch = useDispatch();

  useEffect(() => {
    document.title = "My Space";
  }, []);

  const hello = () => {
    const today = new Date();
    const curHr = today.getHours();
    let sayHi;
    if (curHr < 12) {
      sayHi = "Good Morning";
    } else if (curHr < 18) {
      sayHi = "Good Afternoon";
    } else {
      sayHi = "Good Evening";
    }
    return sayHi;
  };

  const handleBtnUpdate = () => {
    if (validatedData()) {
      fetch(`https://backend-ecommerce-for-gamers.herokuapp.com/customers/${user._id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(updateUser),
      })
        .then((response) => response.json())
        .then((data) => {
          toast.success("Your information has been updated!", {
            position: "top-center",
            autoClose: 500,
          });
          console.log(data);
          dispatch(loggedInUser(data));
        })
        .catch((error) => {
          toast.error("Your information can't be updated!", {
            position: "top-center",
            autoClose: 500,
          });
          console.error("Error:", error.message);
        });
    }
  };

  //validation data
  const validatedData = () => {
    if (updateUser.fullName === "") {
      toast.error(`Customer's name must be filled!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (updateUser.phone === "") {
      toast.error(`Customer's phone must be filled!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (updateUser.phone.length > 15) {
      toast.error(`Customer's phone invalid!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (updateUser.email === "") {
      toast.error(`Customer's email must be filled!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }
    let regex = new RegExp(
      //eslint-disable-next-line
      "([!#-'*+/-9=?A-Z^-~-]+(.[!#-'*+/-9=?A-Z^-~-]+)*|\"([]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(.[!#-'*+/-9=?A-Z^-~-]+)*|[[\t -Z^-~]*])"
    );
    if (!regex.test(updateUser.email)) {
      toast.error(`Customer's email invalid!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (updateUser.address === "") {
      toast.error(`Customer's address must be filled!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (updateUser.city === "") {
      toast.error(`Customer's city must be filled!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (updateUser.country === "") {
      toast.error(`Customer's country must be filled!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    return true;
  };

  return (
    <Container>
      <h3 className="text-center text-success">
        {hello()}, {user.displayName || user.fullName} <ThumbUp />
      </h3>
      <div className="row">
        <div className="col-md-6 col-sm-12 col-lg-6">
          <h5 className="text-center">My information</h5>
          <div>
            <TextField
              required
              id="fullName"
              label="Full Name"
              fullWidth
              margin="dense"
              value={updateUser.fullName}
              onChange={(e) =>
                setUpdateUser({ ...updateUser, fullName: e.target.value })
              }
            />
            <TextField
              required
              id="phone"
              label="Phone Number"
              fullWidth
              margin="dense"
              value={updateUser.phone}
              onChange={(e) =>
                setUpdateUser({ ...updateUser, phone: e.target.value })
              }
            />
            <TextField
              required
              id="email"
              label="Email"
              fullWidth
              margin="dense"
              value={updateUser.email}
              onChange={(e) =>
                setUpdateUser({ ...updateUser, email: e.target.value })
              }
            />
            <TextField
              required
              id="address"
              label="Address"
              fullWidth
              margin="dense"
              value={updateUser.address}
              onChange={(e) =>
                setUpdateUser({ ...updateUser, address: e.target.value })
              }
            />
            <TextField
              required
              id="address"
              label="City"
              fullWidth
              margin="dense"
              value={updateUser.city}
              onChange={(e) =>
                setUpdateUser({ ...updateUser, city: e.target.value })
              }
            />
            <TextField
              required
              id="address"
              label="Country"
              fullWidth
              margin="dense"
              value={updateUser.country}
              onChange={(e) =>
                setUpdateUser({ ...updateUser, country: e.target.value })
              }
            />
          </div>
          <div className="d-flex justify-content-center">
            <button className="btn btn-primary" onClick={handleBtnUpdate}>
              Update Info
            </button>
          </div>
        </div>
        <div className="col-md-6 col-lg-6 col-sm-12">
          <h5 className="text-center text-danger">My Order History</h5>
          <div>
            {user.orders?.length > 0 ? (
              <Col sm={12} md={12} lg={12}>
                <Table responsive bordered bgcolor="#ffe69c">
                  <thead>
                    <tr style={{ padding: "0 10px" }}>
                      <th>Order Details</th>
                      <th>Total Cost</th>
                      <th>Order Date</th>
                      <th>Order Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    {user.orders.map((ord, index) => {
                      return (
                        <tr key={index}>
                          <td>
                            {ord.orderDetail.map((product) => (
                              <p key={product.id}>
                                {product.name} x{product.quantity}
                              </p>
                            ))}
                          </td>
                          <td>{ord.cost}$</td>
                          <td>{ord.orderDate.slice(0, 10)}</td>
                          <td>Delivered</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              </Col>
            ) : null}
          </div>
        </div>
      </div>
    </Container>
  );
};

export default MySpaceComponent;
