import { Grid, Breadcrumbs } from "@mui/material";
import { NavLink } from 'react-router-dom';

const  BreadcrumbsComponent = ({ routes }) => {
    return (
                <Grid container mt={3}>
                    <Breadcrumbs separator="›" aria-label="breadcrumb" color="black">
                        {
                            routes.map((page, index) => {
                                return (
                                    <NavLink to={page.path} underline="hover" color="inherit" key={index}
                                        style={{ textDecoration: "none", color: "black" }}>
                                        <div className="BreadCrumb">{page.name} </div>
                                    </NavLink>
                                )
                            })
                        }
                    </Breadcrumbs>

                </Grid>
    )
}

export default BreadcrumbsComponent;
