import React, { useEffect } from "react";
import Table from "react-bootstrap/Table";
import {
  faTrashAlt,
  faArrowLeft,
  faWallet,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  removeFromCart,
  clearCart,
  selectToCheckOut,
  selectAllToCheckOut,
  incrementQuantity,
  decrementQuantity,
  processToCheckOut,
} from "../../app/redux/cartSlice";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Bounce, toast } from "react-toastify";
import { Tooltip } from "@mui/material";

const ShoppingCart = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const cartItem = useSelector((state) => state.cart.cartItems);
  const cartCheckOut = useSelector((state) => state.cart.cartCheckOut);
  const total = useSelector((state) => state.cart.cartTotal);

  useEffect(() => {
    document.title = "Your Cart";
  }, []);

  const handleGoBack = () => {
    window.history.back();
  };

  const handleSelectCartItem = (cartItem) => {
    if (cartItem.selected === false) {
      dispatch(selectToCheckOut({ ...cartItem, checked: true }));
    } else {
      dispatch(selectToCheckOut({ ...cartItem, checked: false }));
    }
  };

  const handleSelectAllCart = (e) => {
    e.target.checked
      ? dispatch(selectAllToCheckOut({ checked: true }))
      : dispatch(selectAllToCheckOut({ checked: false }));
  };

  const handleRemoveFromCart = (cartItem) => {
    dispatch(removeFromCart(cartItem));
  };

  const handleClearCart = () => {
    dispatch(clearCart());
  };

  const handleCheckOut = () => {
    if (!cartCheckOut.length <= 0) {
      navigate("/cart/checkout");
      dispatch(processToCheckOut({ selected: true }));
    } else {
      toast.error("You must select at least an item!", {
        position: "top-center",
        autoClose: 500,
        transition: Bounce,
        hideProgressBar: true,
      });
    }
  };

  return (
    <>
      <section>
        {cartItem === null || cartItem.length === 0 ? (
          <div className="jumbotron">
            <h3 className="text-center mb-5">Your cart is empty!</h3>
            <p className="text-center mt-5">
              <button className="btn btn-success mt-2" onClick={handleGoBack}>
                Go Back and Buy Something!
              </button>
            </p>
          </div>
        ) : (
          <>
            <div className="jumbotron">
              <h3 className="text-center mb-5">Your shopping cart</h3>
            </div>
            <div className="row mt-3">
              <Table responsive bordered>
                <thead>
                  <tr className="text-center">
                    <th scope="col">
                      <span>
                        <input
                          type={"checkbox"}
                          onClick={handleSelectAllCart}
                        />
                      </span>
                      &nbsp;Select All
                    </th>
                    <th scope="col">Img Preview</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Remove</th>
                  </tr>
                </thead>
                <tbody>
                  {cartItem?.map((cartItem) => (
                    <tr
                      key={cartItem._id}
                      style={{
                        textAlign: "center",
                        padding: "10px 50px",
                      }}
                    >
                      <td style={{ padding: "39px 0" }}>
                        <input
                          type={"checkbox"}
                          style={{ width: "20px", height: "20px" }}
                          id={cartItem._id}
                          checked={cartItem.selected}
                          onChange={() => handleSelectCartItem(cartItem)}
                        ></input>
                      </td>
                      <td style={{ padding: "9px 0" }}>
                        <img
                          style={{ maxWidth: "120px", cursor: "pointer" }}
                          className="img-fluid img-thumbnail"
                          src={cartItem.imageUrl}
                          alt="cartItem"
                          onClick={() => navigate(`/products/${cartItem._id}`)}
                        />
                      </td>
                      <td style={{ padding: "39px 0", fontWeight: "bolder" }}>
                        {cartItem.name}
                      </td>
                      <td style={{ padding: "39px 0" }}>
                        {cartItem.buyPrice - cartItem.promotionPrice} $
                      </td>
                      <td style={{ padding: "6px 0" }}>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            borderRadius: "10px",
                          }}
                        >
                          <button
                            style={{
                              border: "none",
                              outline: "none",
                              padding: "0.2rem",
                              background: "none",
                              color: "red",
                            }}
                            onClick={() =>
                              dispatch(decrementQuantity(cartItem))
                            }
                          >
                            -
                          </button>
                          <div style={{ padding: "30px 10px" }}>
                            {cartItem.amount}
                          </div>
                          <button
                            style={{
                              border: "none",
                              outline: "none",
                              padding: "0.2rem",
                              background: "none",
                              color: "green",
                            }}
                            onClick={() =>
                              dispatch(incrementQuantity(cartItem))
                            }
                          >
                            +
                          </button>
                        </div>
                      </td>
                      <td style={{ padding: "39px 0" }}>
                        <Tooltip title="remove">
                          <FontAwesomeIcon
                            icon={faTrashAlt}
                            style={{ cursor: "pointer" }}
                            size={"lg"}
                            color={"red"}
                            onClick={() => handleRemoveFromCart(cartItem)}
                          />
                        </Tooltip>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
              <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                <button
                  className="btn btn-outline-danger me-md-2"
                  onClick={() => handleClearCart()}
                >
                  Clear Cart
                </button>
              </div>
              <p className="border-bottom  d-flex justify-content-end mt-2">
                {cartCheckOut.length} items selected
              </p>
              <h5 className="text-danger d-flex justify-content-end">
                Total: {total.toFixed(2)}$
              </h5>
              <div className="row mt-3 d-flex justify-content-between p-0">
                <button
                  className="btn btn-secondary col-md-2 col-3"
                  style={{ marginLeft: { md: "12px", lg: "12px", sm: "5px" } }}
                  onClick={handleGoBack}
                >
                  <FontAwesomeIcon icon={faArrowLeft} />
                  &nbsp; Continue shopping
                </button>
                <button
                  className="btn btn-danger col-md-2 col-3"
                  onClick={handleCheckOut}
                >
                  Check Out &nbsp; &nbsp; &nbsp;
                  <FontAwesomeIcon icon={faWallet} />
                </button>
              </div>
            </div>
          </>
        )}
      </section>
    </>
  );
};

export default ShoppingCart;
