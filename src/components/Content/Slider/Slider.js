import Carousel from "react-bootstrap/Carousel";
import { useNavigate } from "react-router-dom";

function Slide() {
  const navigate = useNavigate();
  return (
    <Carousel fade>
      <Carousel.Item className="carousel">
        <img
          className="carousel_img"
          style={{
            maxWidth: "50rem",
            maxHeight: "25rem",
            minWidth: "26rem",
            minHeight: "13rem",
            display: "block",
            marginLeft: "auto",
            marginRight: "auto",
            objectFit: "fill",
          }}
          src={require("../../../assets/images/PlayStation5.jpg")}
          alt="First slide"
        />
        <Carousel.Caption>
          <h3>Brand New Play Station 5</h3>
          <button
            className="btn btn-primary"
            onClick={() => navigate("/playstation")}
          >
            Shop Now
          </button>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item className="carousel">
        <img
          className="carousel_img"
          style={{
            maxWidth: "50rem",
            maxHeight: "25rem",
            minWidth: "26rem",
            minHeight: "13rem",
            display: "block",
            marginLeft: "auto",
            marginRight: "auto",
            objectFit: "fill",
          }}
          src={require("../../../assets/images/XboxSeries.jpg")}
          alt="Second slide"
        />
        <Carousel.Caption>
          <h3>New XboxSeries is release!</h3>
          <button className="btn btn-primary" onClick={() => navigate("/xbox")}>
            Shop Now
          </button>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item className="carousel">
        <img
          className="carousel_img"
          style={{
            maxWidth: "50rem",
            maxHeight: "25rem",
            minWidth: "26rem",
            minHeight: "13rem",
            display: "block",
            marginLeft: "auto",
            marginRight: "auto",
            objectFit: "fill",
          }}
          src={require("../../../assets/images/NintendoSwitch.jpg")}
          alt="Third slide"
        />
        <Carousel.Caption>
          <h3>Nintendo Switch for gamers!</h3>
          <button
            className="btn btn-primary"
            onClick={() => navigate("/nintendo")}
          >
            Shop Now
          </button>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default Slide;
