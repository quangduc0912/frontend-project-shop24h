import Card from "react-bootstrap/Card";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ShopByCategories from "./ShopByCategories";

const Content = () => {
  const [products, setProducts] = useState([{}]);
  const navigate = useNavigate();

  const fetchAPI = async (url) => {
    const response = await fetch(url);
    const data = response.json();
    return data;
  };

  useEffect(() => {
    document.title = "Micromania - Gamer's Dream";
    fetchAPI("https://backend-ecommerce-for-gamers.herokuapp.com/products?limit=8")
      .then((data) => {
        console.log(data);
        setProducts(data);
      })
      .catch((error) => console.log(error.message));
  }, []);

  const settings = {
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 2,
    autoplay: true,
    speed: 3900,
    autoplaySpeed: 3900,
    cssEase: "linear",
    pauseOnHover: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <div>
      <div className="container text-center mt-3">
        <h3 className="text-center mb-3">Shop by Categories</h3>
        <ShopByCategories />
        <h3 className="text-center mt-4">New Arrivals</h3>
        <Slider {...settings} className="slider-products">
          {products.map((product, index) => {
            return (
              <Card
                className="shadow rounded"
                style={{ minHeight: "441px" }}
                key={index}
              >
                <Card.Img
                  variant="top"
                  src={product.imageUrl}
                  alt={product.name}
                  style={{
                    cursor: "pointer",
                    height: "15vw",
                    objectFit: "contain",
                  }}
                  onClick={() => {
                    navigate(`/products/${product._id}`);
                  }}
                />
                <Card.Body>
                  <Card.Title
                    style={{
                      fontSize: "15px",
                      whiteSpace: "nowrap",
                      overflow: "hidden",
                      textOverflow: "ellipsis",
                    }}
                  >
                    {product.name}
                  </Card.Title>
                  <Card.Text>
                    <strong
                      className="text-danger"
                      style={{ fontSize: "18px" }}
                    >
                      {product.buyPrice - product.promotionPrice}$&nbsp;&nbsp;
                    </strong>
                    {product.promotionPrice > 0 ? (
                      <del>{product.buyPrice}$</del>
                    ) : null}
                  </Card.Text>
                </Card.Body>
              </Card>
            );
          })}
        </Slider>
      </div>
    </div>
  );
};

export default Content;
