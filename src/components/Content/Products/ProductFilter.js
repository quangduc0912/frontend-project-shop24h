import {
  TextField,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import React, { useState } from "react";
import { useSelector } from "react-redux";

const ProductFilter = ({ setBase_url, setPageIndex }) => {
  const [name, setName] = useState("");
  const [categoryIds, setCategoryIds] = useState([]);
  const [platformIds, setPlatformIds] = useState([]);
  const [minPrice, setMinPrice] = useState("");
  const [maxPrice, setMaxPrice] = useState("");
  const [sortPrice, setSortPrice] = useState("");
  const categories = useSelector((state) => state.category.categories);
  const platforms = useSelector((state) => state.platform.platforms);

  //filter by name
  const handleSearchName = (e) => {
    setMinPrice(0);
    setMaxPrice(0);
    setCategoryIds([]);
    setPlatformIds([]);
    if (e.key === "Enter") {
      setName(e.target.value);
      setBase_url(`https://backend-ecommerce-for-gamers.herokuapp.com/products?name=${name}`);
      setPageIndex(1);
    }
  };

  //filter sort by price
  const handleChangeSort = (e) => {
    setSortPrice(e.target.value);
    setBase_url(`https://backend-ecommerce-for-gamers.herokuapp.com/${e.target.value}`);
    setPageIndex(1);
  };

  //filter category
  const handleSelectCategory = (e) => {
    setName("");
    const currentCategoryChecked = e.target.value;
    const allCategoriesChecked = [...categoryIds];
    const indexFound = allCategoriesChecked.indexOf(currentCategoryChecked);

    let updatedCategoryIds;
    if (indexFound === -1) {
      updatedCategoryIds = [...categoryIds, currentCategoryChecked];
      setCategoryIds(updatedCategoryIds);
    } else {
      updatedCategoryIds = [...categoryIds];
      updatedCategoryIds.splice(indexFound, 1);
      setCategoryIds(updatedCategoryIds);
    }

    setBase_url(
      `https://backend-ecommerce-for-gamers.herokuapp.com/products?categories=${updatedCategoryIds}&platforms=${platformIds}`
    );
    setPageIndex(1);
  };

  //filter platform
  const handleSelectPlatform = (e) => {
    setName("");
    const currentPlatformChecked = e.target.value;
    const allPlatformsChecked = [...platformIds];
    const indexFound = allPlatformsChecked.indexOf(currentPlatformChecked);

    let updatedPlatformIds;
    if (indexFound === -1) {
      updatedPlatformIds = [...platformIds, currentPlatformChecked];
      setPlatformIds(updatedPlatformIds);
    } else {
      updatedPlatformIds = [...platformIds];
      updatedPlatformIds.splice(indexFound, 1);
      setPlatformIds(updatedPlatformIds);
    }

    setBase_url(
      `https://backend-ecommerce-for-gamers.herokuapp.com/products?categories=${categoryIds}&platforms=${updatedPlatformIds}`
    );
    setPageIndex(1);
  };

  //filter by price
  const handleFilterPrice = (e) => {
    setName("");
    if (e.key === "Enter") {
      setBase_url(
        `https://backend-ecommerce-for-gamers.herokuapp.com/products?minPrice=${minPrice}&maxPrice=${maxPrice}&categories=${categoryIds}&platforms=${platformIds}`
      );
      setPageIndex(1);
    }
  };

  //clear filer
  const handleClearFilter = () => {
    setBase_url(`https://backend-ecommerce-for-gamers.herokuapp.com/products`);
    setPageIndex(1);
    setName("");
    setMinPrice(0);
    setMaxPrice(0);
    setCategoryIds([]);
    setPlatformIds([]);
    setSortPrice("");
  };

  return (
    <div className="row row-cols-sm-1">
      <div className="col">
        <h5 className="mb-2 text-danger">Find product</h5>
        <TextField
          value={name}
          type="text"
          placeholder="enter product's name...."
          variant="standard"
          color="secondary"
          onChange={(e) => setName(e.target.value)}
          onKeyDown={(e) => handleSearchName(e)}
        />
      </div>
      <div className="col">
          <FormControl sx={{ m: 1, minWidth: 160 }}>
            <InputLabel id="demo-simple-select-label">Sort Price</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={sortPrice}
              label="Sort by"
              onChange={handleChangeSort}
            >
              <MenuItem value={"discount"}>Discount</MenuItem>
              <MenuItem value={"priceLowest"}>Low to High</MenuItem>
              <MenuItem value={"priceHighest"}>High to Low</MenuItem>
            </Select>
          </FormControl>
      </div>
      <div className="col">
        <h5 className="mb-2 mt-2">Category</h5>
        {categories.map((cat) => (
          <div key={cat.name} className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              value={cat._id}
              id={cat._id}
              checked={categoryIds.includes(cat._id)}
              onChange={handleSelectCategory}
            />
            <label className="form-check-label" htmlFor={cat._id}>
              {cat.name}
            </label>
          </div>
        ))}
      </div>

      <div className="col">
        <h5 className="mb-2 mt-2">Platform</h5>
        {platforms.map((platform) => (
          <div key={platform.name} className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              value={platform._id}
              id={platform._id}
              checked={platformIds.includes(platform._id)}
              onChange={handleSelectPlatform}
            />
            <label className="form-check-label" htmlFor={platform._id}>
              {platform.name}
            </label>
          </div>
        ))}
      </div>
      <div className="d-flex justify-content-center justify-content-md-start mt-2">
        <h5 className="mb-2 text-center text-md-left">Price</h5>
        <div className="col mx-2">
          <div>
            <input
              className="form-control"
              style={{ maxWidth: "60%" }}
              id="priceMin"
              type="text"
              value={minPrice}
              onChange={(e) => setMinPrice(e.target.value)}
            />
          </div>
          <div>
            to
            <input
              className="form-control"
              style={{ width: "60%" }}
              id="priceMax"
              type="text"
              value={maxPrice}
              onChange={(e) => setMaxPrice(e.target.value)}
              onKeyDown={handleFilterPrice}
            />
          </div>
        </div>
      </div>

      <div className="d-flex justify-content-center mt-2 mb-3">
        <button className="btn btn-danger" onClick={handleClearFilter}>
          Clear Filter
        </button>
      </div>
    </div>
  );
};

export default ProductFilter;
