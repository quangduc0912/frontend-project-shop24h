import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import PaymentIcon from "@mui/icons-material/Payment";
import InfoIcon from "@mui/icons-material/Info";
import { Typography, Button } from "@mui/material";
import Rating from "@mui/material/Rating";
import Stack from "@mui/material/Stack";
import IconButton from "@mui/material/IconButton";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { useDispatch } from "react-redux/";
import {
  addToCartSinglePage,
  addToCart,
  buyNowCart,
} from "../../../app/redux/cartSlice";
import { toast } from "react-toastify";

const ProductInfoComponent = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [quantity, setQuantity] = useState(1);
  const { productId } = useParams();
  const [productInfo, setProductInfo] = useState(null);
  const [productSimilar, setProductSimilar] = useState([]);
  const [title, setTitle] = useState("");

  const getSimilarProducts = async (data) => {
    await fetch(`https://backend-ecommerce-for-gamers.herokuapp.com/products?limit=4&categories=${data}`)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setProductSimilar(data);
      })
      .catch((err) => console.log(err.message));
  };

  const handleGoToCategoryPage = (e) => {
    const value = e.target.firstChild.textContent;
    if (value === "Video Games") {
      navigate("/videogames");
    }
    if (value === "Console Gaming") {
      navigate("/console");
    }
    if (value === "Accessorizes") {
      navigate("/accessorizes");
    }
  };

  const handleGoToPlatformPage = (e) => {
    const value = e.target.firstChild.textContent;
    if (value === "Playstation") {
      navigate("/playstation");
    }
    if (value === "Xbox") {
      navigate("/xbox");
    }
    if (value === "Nintendo") {
      navigate("/nintendo");
    }
     if (value === "PC Game") {
      navigate("/pc");
    }
  };

  useEffect(() => {
    document.title = title;
    const getInfoProduct = async () => {
      await fetch(`https://backend-ecommerce-for-gamers.herokuapp.com/products/${productId}`)
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          setProductInfo(data);
          getSimilarProducts(data.category._id);
          setQuantity(1);
          setTitle(data.name);
          window.scrollTo(0, 0);
        })
        .catch((err) => console.log(err.message));
    };

    getInfoProduct();
  }, [productId, title]);

  const handleIncrement = () => {
    setQuantity(quantity + 1);
  };

  const handleDecrement = () => {
    if (quantity !== 0) {
      setQuantity(quantity - 1);
    }
  };

  const handleAddToCart = () => {
    if (quantity !== 0) {
      dispatch(addToCartSinglePage({ ...productInfo, quantity }));
    } else {
      toast.warning("Please chose the product's quantity!", {
        position: "top-center",
        autoClose: 500,
      });
    }
  };

  const handleBuyNow = () => {
    dispatch(buyNowCart({ ...productInfo, quantity }));
    navigate("/cart");
  };

  return (
    <div>
      {/* <!-- content top --> */}
      {productInfo !== null ? (
        <div className="container border-bottom">
          <div className="row mt-5">
            <div className="col-md-6 col-sm-6">
              <img src={productInfo.imageUrl} width="100%" alt="" />
            </div>
            <div className="col-md-6 col-sm-12">
              <div>
                <h3>{productInfo.name}</h3>
                <p className="mt-3" style={{ alignItems: "center" }}>
                  Rated: <Rating size="small" value={4} readOnly />{" "}
                </p>
              </div>
              <div className="mt-3">
                <h4 className="text-danger">
                  {productInfo.buyPrice - productInfo.promotionPrice} $
                </h4>
              </div>
              <div className="mt-3 mb-3">
                <span className="mx-2">
                  Category :{" "}
                  <span
                    style={{ fontWeight: "bolder", cursor: "pointer" }}
                    onClick={handleGoToCategoryPage}
                  >
                    {productInfo.category.name}
                  </span>
                </span>
                <span className="mx-2">
                  Platform :{" "}
                  <span
                    style={{ fontWeight: "bolder", cursor: "pointer" }}
                    onClick={handleGoToPlatformPage}
                  >
                    {productInfo.platform.name}
                  </span>
                </span>
              </div>
              <h4 className="mt-2">Description</h4>
              <div className="col-sm-4 col-md-12 mb-3">
                <span>{productInfo.description}</span>
              </div>
              <Stack direction="row" spacing={1} style={{ height: "20px" }}>
                <IconButton
                  color="primary"
                  aria-label="add a product"
                  style={{ height: "20px", width: "20px" }}
                  onClick={handleIncrement}
                >
                  <AddIcon />
                </IconButton>
                <input
                  style={{ width: "20px", border: "none" }}
                  readOnly
                  value={quantity}
                />
                <IconButton
                  color="primary"
                  aria-label="remove a product"
                  style={{ height: "20px", width: "20px" }}
                  onClick={handleDecrement}
                >
                  <RemoveIcon />
                </IconButton>
              </Stack>
              <div className="row mt-4">
                <button
                  className="btn btn-outline-primary col-sm-3 col-md-4 shadow rounded mb-2 mx-lg-2 mx-md-2"
                  onClick={handleAddToCart}
                >
                  ADD TO CART&nbsp;
                  <AddShoppingCartIcon />
                </button>
                <button
                  className="btn btn-danger col-sm-3 col-md-4 shadow rounded mb-2"
                  onClick={handleBuyNow}
                >
                  BUY NOW&nbsp;
                  <PaymentIcon />
                </button>
              </div>
            </div>
          </div>
        </div>
      ) : null}

      {/* <!-- content bottom --> */}
      <div className="container mb-5">
        <div className="text-center mt-4">
          <h4>Related Products</h4>
        </div>
        <div className="row mt-3 d-flex justify-content-center text-center">
          {productSimilar.map((product, index) => {
            return (
              <div
                className="card mx-2 my-2"
                style={{ width: "15rem" }}
                key={index}
              >
                <img
                  src={product.imageUrl}
                  className="card-img-top"
                  alt={product.name}
                  style={{
                    cursor: "pointer",
                    height: "15vw",
                    objectFit: "contain",
                  }}
                  data-toggle="tooltip"
                  title={product.name}
                  onClick={() => {
                    navigate(`/products/${product._id}`);
                  }}
                />
                <div className="card-body text-center">
                  <h5 className="card-title">{product.name}</h5>
                  <Typography mt={1}>
                    <strong className="text-danger">
                      {product.buyPrice - product.promotionPrice}$&nbsp;&nbsp;
                    </strong>
                    {product.promotionPrice > 0 ? (
                      <del>{product.buyPrice}$</del>
                    ) : null}
                  </Typography>
                </div>
                <div>
                  <div className="row justify-content-center">
                    <Button
                      variant="contained"
                      style={{
                        width: "100px",
                        height: "30px",
                        marginRight: "5px",
                      }}
                      data-toggle="tooltip"
                      title="Add to Cart"
                      onClick={() => dispatch(addToCart(product))}
                    >
                      <AddShoppingCartIcon />
                    </Button>
                    <Button
                      variant="contained"
                      color="success"
                      style={{
                        width: "100px",
                        height: "30px",
                      }}
                      data-toggle="tooltip"
                      title="Get More Info"
                      onClick={() => {
                        navigate(`/products/${product._id}`);
                      }}
                    >
                      <InfoIcon className="mx-1" />
                      Info
                    </Button>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default ProductInfoComponent;
