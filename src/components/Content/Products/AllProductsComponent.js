import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import ProductFilter from "./ProductFilter";
import { Button, Typography, Stack, Pagination } from "@mui/material";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import InfoIcon from "@mui/icons-material/Info";
import { useDispatch } from "react-redux";
import { addToCart } from "../../../app/redux/cartSlice";
import { getCategories } from "../../../app/redux/categorySlice";
import { getPlatforms } from "../../../app/redux/platformSlice";

const AllProductsComponent = () => {
  const [products, setProducts] = useState([{}]);
  const [noPage, setNoPage] = useState(5);
  const [pageIndex, setPageIndex] = useState(1);
  const [base_url, setBase_url] = useState(`https://backend-ecommerce-for-gamers.herokuapp.com/products`);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const fetchAPI = async (url) => {
    const response = await fetch(url);
    const data = response.json();
    return data;
  };

  const onPageIndexChange = (event, value) => {
    setPageIndex(value);
  };

  useEffect(() => {
    dispatch(getCategories());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getPlatforms());
  }, [dispatch]);

  useEffect(() => {
    document.title = "All Products";
    window.scrollTo(0, 0);
    fetchAPI(base_url)
      .then((data) => {
        console.log(data);
        setNoPage(Math.ceil(data.length / 15));
        setProducts(data.slice((pageIndex - 1) * 15, pageIndex * 15));
      })
      .catch((error) => console.log(error.message));
  }, [base_url, pageIndex, noPage]);

  return (
    <>
      <div className="mt-3 row justify-content-between">
        <div className="col-md-2 col-sm-2">
          <ProductFilter
            setBase_url={setBase_url}
            setPageIndex={setPageIndex}
          />
        </div>
        <div className="col-md-10 col-sm-6">
          <div className="row d-flex justify-content-center justify-content-md-start justify-content-lg-start">
            {products.map((product, index) => {
              return (
                <div
                  className="card mx-1 mb-1 shadow rounded"
                  style={{
                    maxWidth: "13rem",
                    maxHeight: "18rem",
                    minHeight: "2rem",
                    minWidth: "0.5rem",
                  }}
                  key={index}
                >
                  <img
                    src={product.imageUrl}
                    className="card-img-top"
                    alt={product.name}
                    style={{
                      cursor: "pointer",
                      height: "15vw",
                      objectFit: "contain",
                    }}
                    data-toggle="tooltip"
                    title={product.name}
                    onClick={() => {
                      navigate(`/products/${product._id}`);
                    }}
                  />
                  <div className="card-body text-center">
                    <Typography
                      component="div"
                      className="card-title"
                      sx={{
                        fontSize: "h6.fontSize",
                        fontWeight: 500,
                        whiteSpace: "nowrap",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                      }}
                    >
                      {product.name}
                    </Typography>
                    <Typography mt={1}>
                      <strong
                        className="text-danger"
                        style={{ fontSize: "20px" }}
                      >
                        {product.buyPrice - product.promotionPrice}$&nbsp;&nbsp;
                      </strong>
                      {product.promotionPrice > 0 ? (
                        <del>{product.buyPrice}$</del>
                      ) : null}
                    </Typography>
                  </div>
                  <div>
                    <div className="row justify-content-center">
                      <Button
                        variant="outlined"
                        style={{
                          width: "100px",
                          height: "30px",
                          marginRight: "5px",
                        }}
                        data-toggle="tooltip"
                        title="Add to Cart"
                        onClick={() => dispatch(addToCart(product))}
                      >
                        <AddShoppingCartIcon />
                      </Button>
                      <Button
                        variant="outlined"
                        color="success"
                        style={{
                          width: "100px",
                          height: "30px",
                        }}
                        data-toggle="tooltip"
                        title="Get More Info"
                        onClick={() => {
                          navigate(`/products/${product._id}`);
                        }}
                      >
                        <InfoIcon className="mx-1" />
                        Info
                      </Button>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
          <Stack
            spacing={2}
            mt={2}
            mb={1}
            direction="row"
            justifyContent={"flex-end"}
          >
            <Pagination
              count={noPage}
              variant="outlined"
              color="success"
              defaultPage={1}
              onChange={onPageIndexChange}
            />
          </Stack>
        </div>
      </div>
    </>
  );
};

export default AllProductsComponent;
