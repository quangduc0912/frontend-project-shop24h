import { TextField } from "@mui/material";
import React, { useEffect, useState } from "react";
import { Col, Container, Form, Table } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Flip, toast, Zoom } from "react-toastify";
import { clearCartCheckOut } from "../../../app/redux/cartSlice";
import { orderSuccess } from "../../../app/redux/orderSlice";

const CheckOut = () => {
  const cartItem = useSelector((state) => state.cart.cartCheckOut);
  const total = useSelector((state) => state.cart.cartTotal);
  const user = useSelector((state) => state.user.user);

  const [shipmentFee, setShipmentFee] = useState(0);
  const [paymentMethod, setPaymentMethod] = useState("");
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    document.title = "Check Out";
  }, []);

  const itemCheckOut = () => {
    let productCheckOut = cartItem.map((product) => {
      return {
        id: product._id,
        name: product.name,
        quantity: product.amount,
        price: (product.buyPrice - product.promotionPrice) * product.amount,
      };
    });
    return productCheckOut;
  };

  const [newOrder, setNewOrder] = useState({
    orderDate: new Date().toISOString().split('T')[0],
    note: "",
    cost: (total + shipmentFee).toFixed(2),
    orderDetail: itemCheckOut(),
    shippedDate: new Date().setDate(new Date().getDate() + 3),
    fullName: user?.displayName || user?.fullName || "",
    receiver: user?.displayName || user?.fullName || "",
    phone: user?.phone || user?.phoneNumber || "",
    email: user?.email || "",
    address: user?.address || "",
    city: "",
    country: "",
  });

  const handlePayment = () => {
    if (validatedData()) {
      console.log(newOrder);
      fetch("https://backend-ecommerce-for-gamers.herokuapp.com/customers/order", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(newOrder),
      })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          if (data.message) {
            toast.warn(`${data.message}`, {
              position: "top-center",
              autoClose: 500,
            });
          } else {
            toast.success("Your order has been created!", {
              position: "top-center",
              autoClose: 500,
              transition: Zoom,
              hideProgressBar: true,
            });
            dispatch(clearCartCheckOut());
            dispatch(orderSuccess(data));
            navigate("/order");
          }
        })
        .catch((error) => {
          toast.error("Failed", {
            position: "top-center",
            autoClose: 500,
          });
          console.error("Error:", error);
        });
    }
  };

  //validate data
  const validatedData = () => {
    if (newOrder.fullName === "") {
      toast.warn("Your Full Name must be filled!", {
        position: "top-center",
        autoClose: 500,
        transition: Flip,
        hideProgressBar: true,
      });
      return false;
    }

    if (newOrder.phone === "") {
      toast.warn("Your Phone Number must be filled!", {
        position: "top-center",
        autoClose: 500,
        transition: Flip,
        hideProgressBar: true,
      });
      return false;
    }

    if (newOrder.email === "") {
      toast.warn("Your Email must be filled!", {
        position: "top-center",
        autoClose: 500,
        transition: Flip,
        hideProgressBar: true,
      });
      return false;
    }

    let regex =
      new RegExp(
      //eslint-disable-next-line
        "([!#-'*+/-9=?A-Z^-~-]+(.[!#-'*+/-9=?A-Z^-~-]+)*|\"([]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(.[!#-'*+/-9=?A-Z^-~-]+)*|[[\t -Z^-~]*])"
      );
    if (!regex.test(newOrder.email)) {
      toast.error(`Your email is invalid!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (newOrder.address === "") {
      toast.warn("Your address must be filled!", {
        position: "top-center",
        autoClose: 500,
        transition: Flip,
        hideProgressBar: true,
      });
      return false;
    }

    if (paymentMethod === "") {
      toast.warn("Please chose your payment method!", {
        position: "top-center",
        autoClose: 500,
        transition: Flip,
        hideProgressBar: true,
      });
      return false;
    }

    return true;
  };

  return (
    <>
      <div className="row mt-3">
        <div className="col-md-12 col-sm-12 col-lg-6 ">
          <h3 className="text-center">Billing Details</h3>
          <div>
            <TextField
              required
              id="fullName"
              label="Full Name"
              fullWidth
              margin="dense"
              defaultValue={newOrder.fullName}
              onChange={(e) =>
                setNewOrder({
                  ...newOrder,
                  fullName: e.target.value,
                  receiver: e.target.value,
                })
              }
            />
            <TextField
              required
              id="phone"
              label="Phone Number"
              fullWidth
              margin="dense"
              defaultValue={newOrder.phone}
              onChange={(e) =>
                setNewOrder({
                  ...newOrder,
                  phone: e.target.value,
                })
              }
            />
            <TextField
              required
              id="email"
              label="Email"
              fullWidth
              margin="dense"
              defaultValue={newOrder.email}
              onChange={(e) =>
                setNewOrder({
                  ...newOrder,
                  email: e.target.value,
                })
              }
            />
            <TextField
              required
              id="address"
              label="Address"
              fullWidth
              margin="dense"
              defaultValue={newOrder.address}
              onChange={(e) =>
                setNewOrder({
                  ...newOrder,
                  address: e.target.value,
                })
              }
            />
            <div className="row">
              <div className="col-sm-6">
                <TextField
                  size="small"
                  id="address"
                  label="City"
                  fullWidth
                  margin="dense"
                  defaultValue={newOrder.city}
                  onChange={(e) =>
                    setNewOrder({
                      ...newOrder,
                      city: e.target.value,
                    })
                  }
                />
              </div>
              <div className="col-sm-6">
                <TextField
                  size="small"
                  id="address"
                  label="Country"
                  fullWidth
                  margin="dense"
                  defaultValue={newOrder.country}
                  onChange={(e) =>
                    setNewOrder({
                      ...newOrder,
                      country: e.target.value,
                    })
                  }
                />
              </div>
            </div>
            <TextField
              id="message"
              label="Message"
              multiline
              rows={4}
              fullWidth
              margin="dense"
              defaultValue={newOrder.note}
              onChange={(e) =>
                setNewOrder({
                  ...newOrder,
                  note: e.target.value,
                })
              }
            />
          </div>
        </div>
        <div className="col-md-12 col-sm-12 col-lg-6">
          <h3 className="text-center mb-3 mt-3">Cart Summary</h3>
          <Container>
            <Table responsive bordered>
              <thead>
                <tr>
                  <th scope="col">Product</th>
                  <th style={{ display: "flex", justifyContent: "center" }}>
                    Price
                  </th>
                </tr>
              </thead>
              <tbody>
                {cartItem.map((product) => {
                  return (
                    <tr key={product._id}>
                      <td>
                        {product.name} x<strong>&nbsp;{product.amount}</strong>
                      </td>
                      <td style={{ display: "flex", justifyContent: "center" }}>
                        {(product.buyPrice - product.promotionPrice) *
                          product.amount}{" "}
                        $
                      </td>
                    </tr>
                  );
                })}
                <tr>
                  <td>Shipping</td>
                  <td style={{ display: "flex", justifyContent: "center" }}>
                    <Form>
                      <Form.Check
                        label="Free (3 - 5 working days)"
                        name="shipping"
                        type="radio"
                        onChange={(e) => setShipmentFee(0)}
                      />
                      <Form.Check
                        label="Fast (under 36h) + 30$"
                        name="shipping"
                        type="radio"
                        onChange={(e) => setShipmentFee(30)}
                      />
                    </Form>
                  </td>
                </tr>
                <tr>
                  <td>
                    <strong>Total</strong>
                  </td>
                  <td style={{ display: "flex", justifyContent: "center" }}>
                    <span className="text-danger">
                      {(total + shipmentFee).toFixed(2)} $
                    </span>
                  </td>
                </tr>
              </tbody>
            </Table>
          </Container>
          <Container>
            <h5 className="mb-2">Please chose your payment method</h5>
            <Col>
              <Form>
                <Form.Check
                  label="Direct Bank Transfer"
                  name="payment"
                  type="radio"
                  value="Bank"
                  onChange={(e) => setPaymentMethod(e.target.value)}
                />
                <Form.Check
                  label="Cash on Delivered"
                  name="payment"
                  type="radio"
                  value="COD"
                  onChange={(e) => setPaymentMethod(e.target.value)}
                />
                <Form.Check
                  label="Credit Card"
                  name="payment"
                  type="radio"
                  value="CC"
                  onChange={(e) => setPaymentMethod(e.target.value)}
                />
              </Form>
            </Col>
          </Container>
          {user === "" ? (
            <p className="text-center mt-2">
              Doesn't have account? Click{" "}
              <span
                style={{
                  color: "#0d6efd",
                  cursor: "pointer",
                }}
                onClick={() => navigate("/signup")}
              >
                here
              </span>{" "}
              to create for new customer!
            </p>
          ) : null}
          <Container
            style={{
              display: "flex",
              justifyContent: "center",
              marginTop: "10px",
            }}
          >
            <button className="btn btn-success" onClick={handlePayment}>
              Place Order
            </button>
          </Container>
        </div>
      </div>
    </>
  );
};

export default CheckOut;
