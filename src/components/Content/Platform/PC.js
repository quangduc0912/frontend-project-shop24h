import React, { useState, useEffect } from "react";
import Card from "react-bootstrap/Card";
import { useNavigate } from "react-router-dom";
import Slider from "react-slick";

const PCGamingComponent = () => {
  const [videos, setVideos] = useState([]);
  const [consoles, setConsoles] = useState([]);
  const [accessorizes, setAccessorizes] = useState([]);

  const navigate = useNavigate();

  useEffect(() => {
    document.title = "PC Gaming's House";
    const getCategoryVideos = async () => {
      await fetch(
        "https://backend-ecommerce-for-gamers.herokuapp.com/products?platforms=62da1596b66ac0acd95d4240&categories=62da13097c837a7d3ca28d4a"
      )
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          setVideos(data);
        })
        .catch((err) => console.log(err.message));
    };

    const getCategoryConsoles = async () => {
      await fetch(
        "https://backend-ecommerce-for-gamers.herokuapp.com/products?platforms=62da1596b66ac0acd95d4240&categories=62d981c670db2be556175020"
      )
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          setConsoles(data);
        })
        .catch((err) => console.log(err.message));
    };

    const getCategoryAccessorizes = async () => {
      await fetch(
        "https://backend-ecommerce-for-gamers.herokuapp.com/products?platforms=62da1596b66ac0acd95d4240&categories=62da12dd7c837a7d3ca28d48"
      )
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          setAccessorizes(data);
        })
        .catch((err) => console.log(err.message));
    };

    getCategoryConsoles();
    getCategoryVideos();
    getCategoryAccessorizes();
  }, []);

  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    autoplay: true,
    autoplaySpeed: 2000,
    cssEase: "linear",
    pauseOnHover: true,
    slidesToShow: 6,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: false,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  const settingSlide = {
    className: "center",
    centerMode: true,
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <>
      <div className="mt-3 justify-content-between">
        <div>
          <Slider {...settingSlide}>
            <Card className="bg-dark text-white">
              <Card.Img
                src={require("../../../assets/images/pc.jpg")}
                alt="Card image"
                style={{
                  maxWidth: "30rem",
                  maxHeight: "20rem",
                  minWidth: "12rem",
                  minHeight: "6rem",
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
              <Card.ImgOverlay>
                <Card.Title
                  className="text-center"
                  style={{ fontSize: "2vw", opacity: "0.7" }}
                >
                  All PC Gaming Products
                </Card.Title>
              </Card.ImgOverlay>
            </Card>
          </Slider>
        </div>
        <div className="container text-center">
          <h3 className="d-flex justify-content-start mt-3 border-bottom">
            Console
          </h3>
          <Slider {...settings} className="slider-products">
            {consoles.map((product, index) => {
              return (
                <Card
                  className="shadow rounded"
                  style={{ minHeight: "441px" }}
                  key={index}
                >
                  <Card.Img
                    variant="top"
                    src={product.imageUrl}
                    alt={product.name}
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      navigate(`/products/${product._id}`);
                    }}
                  />
                  <Card.Body>
                    <Card.Title
                      style={{
                        fontSize: "15px",
                        whiteSpace: "nowrap",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                      }}
                    >
                      {product.name}
                    </Card.Title>
                    <Card.Text>
                      <strong
                        className="text-danger"
                        style={{ fontSize: "18px" }}
                      >
                        {product.buyPrice - product.promotionPrice}$&nbsp;&nbsp;
                      </strong>
                      {product.promotionPrice > 0 ? (
                        <del>{product.buyPrice}$</del>
                      ) : null}
                    </Card.Text>
                  </Card.Body>
                </Card>
              );
            })}
          </Slider>
          <h3 className="d-flex justify-content-start mt-4 border-bottom">
            Videos Game
          </h3>
          <Slider {...settings} className="slider-products">
            {videos.map((product, index) => {
              return (
                <Card
                  className="shadow rounded"
                  style={{ minHeight: "441px" }}
                  key={index}
                >
                  <Card.Img
                    variant="top"
                    src={product.imageUrl}
                    alt={product.name}
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      navigate(`/products/${product._id}`);
                    }}
                  />
                  <Card.Body>
                    <Card.Title
                      style={{
                        fontSize: "15px",
                        whiteSpace: "nowrap",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                      }}
                    >
                      {product.name}
                    </Card.Title>
                    <Card.Text>
                      <strong
                        className="text-danger"
                        style={{ fontSize: "18px" }}
                      >
                        {product.buyPrice - product.promotionPrice}$&nbsp;&nbsp;
                      </strong>
                      {product.promotionPrice > 0 ? (
                        <del>{product.buyPrice}$</del>
                      ) : null}
                    </Card.Text>
                  </Card.Body>
                </Card>
              );
            })}
          </Slider>
          <h3 className="d-flex justify-content-start mt-4 border-bottom">
            Accessorizes
          </h3>
          <Slider {...settings} className="slider-products">
            {accessorizes.map((product, index) => {
              return (
                <Card
                  className="shadow rounded mb-1"
                  style={{ minHeight: "441px" }}
                  key={index}
                >
                  <Card.Img
                    variant="top"
                    src={product.imageUrl}
                    alt={product.name}
                    style={{
                      cursor: "pointer",
                      height: "15vw",
                      objectFit: "contain",
                    }}
                    onClick={() => {
                      navigate(`/products/${product._id}`);
                    }}
                  />
                  <Card.Body>
                    <Card.Title
                      style={{
                        fontSize: "15px",
                        whiteSpace: "nowrap",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                      }}
                    >
                      {product.name}
                    </Card.Title>
                    <Card.Text>
                      <strong
                        className="text-danger"
                        style={{ fontSize: "18px" }}
                      >
                        {product.buyPrice - product.promotionPrice}$&nbsp;&nbsp;
                      </strong>
                      {product.promotionPrice > 0 ? (
                        <del>{product.buyPrice}$</del>
                      ) : null}
                    </Card.Text>
                  </Card.Body>
                </Card>
              );
            })}
          </Slider>
        </div>
      </div>
    </>
  );
};

export default PCGamingComponent;
