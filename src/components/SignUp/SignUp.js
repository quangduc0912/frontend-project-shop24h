import React, { useEffect, useState } from "react";
import { TextField } from "@mui/material";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";
import { loggedInUser } from "../../app/redux/userSlice";

const SignUp = () => {
  const [newCustomer, setNewCustomer] = useState({
    fullName: "",
    phone: "",
    email: "",
    address: "",
    city: "",
    country: "",
  });

  const dispatch = useDispatch();

  const handleBtnSignUp = () => {
    if (validatedData()) {
      fetch(`https://backend-ecommerce-for-gamers.herokuapp.com/customers/`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(newCustomer),
      })
        .then((response) => response.json())
        .then((data) => {
          toast.success("New Customer has been created!", {
            position: "top-center",
            autoClose: 500,
          });
          console.log(data);
          dispatch(loggedInUser(data));
          window.history.back()
        })
        .catch((error) => {
          toast.error("New Customer can't be created!", {
            position: "top-center",
            autoClose: 500,
          });
          console.error("Error:", error.message);
        });
    }
  };

  //validation data
  const validatedData = () => {
    if (newCustomer.fullName === "") {
      toast.error(`Customer's name must be filled!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (newCustomer.phone === "") {
      toast.error(`Customer's phone must be filled!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (newCustomer.phone.length > 15) {
      toast.error(`Customer's phone invalid!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (newCustomer.email === "") {
      toast.error(`Customer's email must be filled!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (
     // eslint-disable-next-line
      !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(newCustomer.email)
    ) {
      toast.error(`Customer's email invalid!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (newCustomer.address === "") {
      toast.error(`Customer's address must be filled!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (newCustomer.city === "") {
      toast.error(`Customer's city must be filled!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    if (newCustomer.country === "") {
      toast.error(`Customer's country must be filled!`, {
        position: "top-center",
        autoClose: 500,
      });
      return false;
    }

    return true;
  };

  useEffect(() => {
    document.title = "Sign Up";
  }, []);

  return (
    <div className="d-flex justify-content-center">
      <div className="col-md-6 col-sm-12 col-lg-6">
        <h3 className="text-center">New Customer Sign Up</h3>
        <div>
          <TextField
            required
            id="fullName"
            label="Full Name"
            fullWidth
            margin="dense"
            value={newCustomer.fullName}
            onChange={(e) =>
              setNewCustomer({ ...newCustomer, fullName: e.target.value })
            }
          />
          <TextField
            required
            id="phone"
            label="Phone Number"
            fullWidth
            margin="dense"
            value={newCustomer.phone}
            onChange={(e) =>
              setNewCustomer({ ...newCustomer, phone: e.target.value })
            }
          />
          <TextField
            required
            id="email"
            label="Email"
            fullWidth
            margin="dense"
            value={newCustomer.email}
            onChange={(e) =>
              setNewCustomer({ ...newCustomer, email: e.target.value })
            }
          />
          <TextField
            required
            id="address"
            label="Address"
            fullWidth
            margin="dense"
            value={newCustomer.address}
            onChange={(e) =>
              setNewCustomer({ ...newCustomer, address: e.target.value })
            }
          />
          <TextField
            required
            id="address"
            label="City"
            fullWidth
            margin="dense"
            value={newCustomer.city}
            onChange={(e) =>
              setNewCustomer({ ...newCustomer, city: e.target.value })
            }
          />
          <TextField
            required
            id="address"
            label="Country"
            fullWidth
            margin="dense"
            value={newCustomer.country}
            onChange={(e) =>
              setNewCustomer({ ...newCustomer, country: e.target.value })
            }
          />
        </div>
        <div className="d-flex justify-content-center">
          <button className="btn btn-success" onClick={handleBtnSignUp}>
            Sign Up
          </button>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
