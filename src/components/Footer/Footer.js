import { Grid, Typography, Link } from "@mui/material";
import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import YouTubeIcon from "@mui/icons-material/YouTube";
import TwitterIcon from "@mui/icons-material/Twitter";

function Footer() {
  const title = [
    "Help Center",
    "Contact Us",
    "Product Help",
    "Warranty",
    "Order Status",
  ];
  return (
    <div className="mt-5 container-fluid footer">
      <Grid
        container
        fluid="true"
        spacing={1}
        style={{
          backgroundImage:
            "linear-gradient(90deg,#164094 0,#164094 38%,#52ae32 0)",
          color: "white",
        }}
      >
        <Grid item xs={3} sm={6} md={2} lg={2}>
          <Typography variant="h6 || inherit">
            <b>PRODUCTS</b>
          </Typography>
          {title.map(function (element, index) {
            return (
              <Grid item xs={12} key={index}>
                <Typography variant="caption">{element}</Typography>
              </Grid>
            );
          })}
        </Grid>

        <Grid
          item
          xs={3}
          sm={6}
          md={2}
          lg={2}
          sx={{ mr: { md: 3, lg: 3, sm: 0 } }}
        >
          <Typography variant="h6 || inherit">
            <b>SERVICES</b>
          </Typography>
          {title.map(function (element, index) {
            return (
              <Grid item xs={12} key={index}>
                <Typography variant="caption">{element}</Typography>
              </Grid>
            );
          })}
        </Grid>

        <Grid item xs={3} sm={6} md={2} lg={2}>
          <Typography variant="h6 || inherit">
            <b>SUPPORT</b>
          </Typography>
          {title.map(function (element, index) {
            return (
              <Grid item xs={12} key={index}>
                <Typography variant="caption">{element}</Typography>
              </Grid>
            );
          })}
        </Grid>

        <Grid item xs={3} sm={6} md={5} lg={5} align="center" className="pt-4">
          <Typography sx={{ fontSize: { sm: 25, md: 35, lg: 45 } }}>
            <b>Gaming Shop 24h</b>
          </Typography>
          <Grid container className="d-flex justify-content-center mt-3">
            <Link
              href="https://www.facebook.com/"
              sx={{ mr: 2, color: "black" }}
            >
              <FacebookIcon />
            </Link>
            <Link
              href="https://www.instagram.com/"
              sx={{ mr: 2, color: "black" }}
            >
              <InstagramIcon />
            </Link>
            <Link
              href="https://www.youtube.com/"
              sx={{ mr: 2, color: "black" }}
            >
              <YouTubeIcon />
            </Link>
            <Link href="https://twitter.com/" sx={{ color: "black" }}>
              <TwitterIcon />
            </Link>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default Footer;
