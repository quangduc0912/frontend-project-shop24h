import { Container, Navbar, Nav } from "react-bootstrap/";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Badge from "@mui/material/Badge";
import {
  faUser,
  faSignOut,
  faGamepad,
  faComputer,
  faStore,
} from "@fortawesome/free-solid-svg-icons";
import { faPlaystation, faXbox } from "@fortawesome/free-brands-svg-icons";
import LocalMallIcon from "@mui/icons-material/LocalMall";
import LogInModal from "./LogInModal";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { loggedInUser } from "../../app/redux/userSlice";
import { auth } from "../../data/firebase";
import { toast } from "react-toastify";

const imgLogo =
  "https://www.micromania.fr/on/demandware.static/Sites-Micromania-Site/-/default/dw4bd9542a/images/logos.svg";

function Header() {
  const cartItems = useSelector((state) => state.cart.cartItems);
  const user = useSelector((state) => state.user.user);
  const [modalShow, setModalShow] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const logOutGoogle = () => {
    auth
      .signOut()
      .then((result) => {
        dispatch(loggedInUser(null));
        navigate("/");
        toast.warning("Logged Out successfully!", {
          position: "top-center",
          autoClose: 500,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <Navbar
      collapseOnSelect
      expand="md"
      variant="light"
      fixed="top"
      style={{
        backgroundImage:
          "linear-gradient(90deg,#164094 0,#164094 38%,#52ae32 0)",
      }}
    >
      <Container className="justify-content-center">
        <Navbar.Brand>
          <img
            src={imgLogo}
            alt="img logo"
            style={{ cursor: "pointer" }}
            onClick={() => {
              navigate("/");
            }}
          ></img>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto" style={{ alignItems: "center" }}>
            <Nav.Link
              className="text-white"
              onClick={() => navigate("/products")}
              data-toggle="tooltip"
              title="All Products"
            >
              <FontAwesomeIcon
                icon={faStore}
                style={{ marginRight: "3px" }}
                onClick={() => navigate("/products")}
              />
              All Products
            </Nav.Link>
            <Nav.Link
              className="text-white"
              onClick={() => navigate("/playstation")}
              data-toggle="tooltip"
              title="Playstation's Products"
            >
              <FontAwesomeIcon
                icon={faPlaystation}
                style={{ marginRight: "3px" }}
                onClick={() => navigate("/playstation")}
              />
              Playstation
            </Nav.Link>
            <Nav.Link
              className="text-white"
              onClick={() => navigate("/xbox")}
              data-toggle="tooltip"
              title="Xbox's Products"
            >
              <FontAwesomeIcon
                icon={faXbox}
                style={{ marginRight: "3px" }}
                onClick={() => navigate("/xbox")}
              />
              Xbox
            </Nav.Link>

            <Nav.Link
              className="text-white"
              onClick={() => navigate("/nintendo")}
              data-toggle="tooltip"
              title="Nintendo's Products"
            >
              <FontAwesomeIcon
                icon={faGamepad}
                style={{ marginRight: "3px" }}
                onClick={() => navigate("/nintendo")}
              />
              Nintendo
            </Nav.Link>
            <Nav.Link
              className="text-white"
              onClick={() => navigate("/pc")}
              data-toggle="tooltip"
              title="PC's Products"
            >
              <FontAwesomeIcon
                icon={faComputer}
                style={{ marginRight: "3px" }}
                onClick={() => navigate("/pc")}
              />
              PC
            </Nav.Link>
          </Nav>

          <Nav style={{ alignItems: "center" }}>
            <Nav.Link
              className="mx-1"
              style={{ margin: "0,1rem 0rem" }}
              data-toggle="tooltip"
              title="Shopping Cart"
            >
              <Badge badgeContent={cartItems.length} color="info">
                <LocalMallIcon
                  style={{ color: "white" }}
                  onClick={() => navigate("/cart")}
                />
              </Badge>
            </Nav.Link>
            <Nav.Link className="mx-1">
              {user ? (
                <img
                  src={user.photoURL}
                  width={30}
                  height={30}
                  alt="avatar"
                  style={{
                    color: "white",
                    borderRadius: "50%",
                  }}
                  data-toggle="tooltip"
                  title="My Profile"
                  onClick={() => setModalShow(true)}
                />
              ) : (
                <FontAwesomeIcon
                  icon={faUser}
                  size={"lg"}
                  style={{ color: "white", paddingTop: "4px" }}
                  data-toggle="tooltip"
                  title="Log In"
                  onClick={() => setModalShow(true)}
                />
              )}
            </Nav.Link>
            {user ? (
              <Nav.Link className="mx-1">
                <FontAwesomeIcon
                  icon={faSignOut}
                  size={"lg"}
                  style={{ color: "white", paddingTop: "4px" }}
                  data-toggle="tooltip"
                  title="Sign Out"
                  onClick={logOutGoogle}
                />
              </Nav.Link>
            ) : null}
          </Nav>
        </Navbar.Collapse>

        {/* Modal Login */}
        <LogInModal
          show={modalShow}
          onHide={() => setModalShow(false)}
          setModalShow={setModalShow}
        />
      </Container>
    </Navbar>
  );
}

export default Header;
