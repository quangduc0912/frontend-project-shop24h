import React, { useEffect, useState } from "react";
import { Modal, Button } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loggedInUser } from "../../app/redux/userSlice";
import { auth, googleProvider } from "../../data/firebase";
import { toast } from "react-toastify";

const LogInModal = ({ show, setModalShow, ...props }) => {
  const user = useSelector((state) => state.user.user);
  const [phoneNumber, setPhoneNumber] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const fetchAPI = async (url) => {
    const response = await fetch(url);
    const data = response.json();
    return data;
  };

  const logInByPhone = () => {
    fetchAPI(`https://backend-ecommerce-for-gamers.herokuapp.com/customers/phone/${phoneNumber}`)
      .then((data) => {
        if (data.message) {
          toast.warn("Can't sign in!", {
            position: "top-right",
            autoClose: 500,
          });
        } else {
          toast.success("Sign In Successfully!", {
            position: "top-right",
            autoClose: 500,
          });
          dispatch(loggedInUser(data));
        }
      })
      .catch((error) => {
        toast.warn("Can't sign in!", {
          position: "top-right",
          autoClose: 500,
        });
        console.log(error.message);
      });
  };

  const logInGoogle = () => {
    auth
      .signInWithPopup(googleProvider)
      .then((result) => {
        toast.success("Sign In Successfully!", {
          position: "top-right",
          autoClose: 500,
        });
        dispatch(loggedInUser(result.user));
      })
      .catch((err) => {
        toast.warn("Can't sign in!", {
          position: "top-right",
          autoClose: 500,
        });
        console.log(err);
      });
  };

  useEffect(() => {
    auth.onAuthStateChanged((result) => {
      if(result !== null) dispatch(loggedInUser(result.providerData[0])) 
    });
  }, [dispatch]);

  return (
    <Modal {...props} size={"md" || "sm"} show={show} centered>
      {user ? (
        <>
          <Modal.Header style={{ justifyContent: "center" }}>
            <img
              src={user.photoURL}
              width={50}
              height={50}
              alt="avatar"
              style={{ borderRadius: "50%" }}
            />
          </Modal.Header>
          <Modal.Body>
            <div className="text-center my-3">
              <h3>Hi, {user.displayName || user.fullName}</h3>
            </div>
            <div className="text-center mt-5">
              <Button
                className="form-control"
                style={{
                  backgroundColor: "#feae96",
                  backgroundImage:
                    "linear-gradient(315deg, #feae96 0%, #fe0944 74%)",
                  borderRadius: "30px",
                  padding: "12px",
                }}
                onClick={() => navigate("/myspace")}
              >
                Go to my Space
              </Button>
            </div>
          </Modal.Body>
        </>
      ) : (
        <>
          <Modal.Header style={{ justifyContent: "center" }}>
            <h3>Log In</h3>
          </Modal.Header>
          <Modal.Body>
            <div>
              <input
                placeholder="phone number..."
                className="form-control w-70 my-2"
                value={phoneNumber}
                onChange={(e) => setPhoneNumber(e.target.value)}
              />
              <input
                placeholder="password"
                type="password"
                className="form-control w-70 my-3"
              />
              <p className="font-small d-flex justify-content-end">
                <span
                  style={{
                    color: "#0d6efd",
                    cursor: "pointer",
                  }}
                >
                  Forgot Password?
                </span>
              </p>
            </div>
            <div className="text-center my-2">
              <Button
                className="form-control"
                style={{
                  background: "linear-gradient(40deg,#45cafc,#303f9f)",
                  borderRadius: "30px",
                  padding: "12px",
                }}
                onClick={logInByPhone}
              >
                SIGN IN
              </Button>
            </div>
            <p className="d-flex justify-content-center my-3">
              or Sign In with:
            </p>
            <div className="text-center my-2">
              <Button
                className="form-control"
                style={{
                  backgroundColor: "#feae96",
                  backgroundImage:
                    "linear-gradient(315deg, #feae96 0%, #fe0944 74%)",
                  borderRadius: "30px",
                  padding: "12px",
                }}
                onClick={logInGoogle}
              >
                GOOGLE
              </Button>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <p
              className="font-small d-flex justify-content-end"
              style={{ color: "GrayText" }}
            >
              Not a member?
              <span
                style={{
                  color: "#0d6efd",
                  marginLeft: "3px",
                  cursor: "pointer",
                }}
                onClick={() => navigate("/signup")}
              >
                Sign Up
              </span>
            </p>
          </Modal.Footer>
        </>
      )}
    </Modal>
  );
};

export default LogInModal;
