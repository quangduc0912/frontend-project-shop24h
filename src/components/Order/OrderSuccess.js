import React, { useEffect } from "react";
import { Container, Col, Button, Table, Row } from "react-bootstrap";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const OrderSuccess = () => {
  const order = useSelector((state) => state.order.order);
  const orderId = order.orders.length
  const navigate = useNavigate();

  useEffect(() => {
    document.title = "Your Order";
  }, []);

  return (
    <>
      <Container>
        <Row>
          <h2 className="text-success text-center mt-1 mb-3">
            Thank you for your purchase!
          </h2>
          <Col sm={12} md={4} lg={4}>
            <p className="mt-2">
              Your orderID is:{" "}
              <span className="text-danger">{orderId > 0 ? order.orders[orderId - 1]._id : null}</span>
            </p>
            <Col className="mt-3">
              <Button variant="primary" onClick={() => navigate("/products")}>
                Continue Shopping
              </Button>
            </Col>
          </Col>
          <Col sm={12} md={8} lg={8}>
            <h5 className="mt-2">Order Information</h5>
            <Table responsive bordered bgcolor="#ffe69c">
              <thead>
                <tr style={{ padding: "0 10px" }}>
                  <th>Order Details</th>
                  <th>Total Cost</th>
                  <th>Shipping Information</th>
                  <th>Order Date</th>
                  <th>Order Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    {order.orders[orderId - 1].orderDetail.map((product,index) => (
                      <p key={index}>
                        {product.name} x{product.amount}
                      </p>
                    ))}
                  </td>
                  <td>{order.orders[orderId -1].cost}$</td>
                  <td>
                    to {order.fullName}, {order.phone}, {order.address}
                  </td>
                  <td>{order.orders[orderId - 1].orderDate.slice(0, 10)}</td>
                  <td>packing...</td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default OrderSuccess;
