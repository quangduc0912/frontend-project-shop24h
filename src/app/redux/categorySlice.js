import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios"; 

export const getCategories = createAsyncThunk('category/getCategories', async() => {
    try {
        const response = await axios.get("https://backend-ecommerce-for-gamers.herokuapp.com/categories");
        return response.data
    }
    catch(err) {
        return err.message
    }
})


const category = createSlice({
    name: 'category',
    initialState: {
        categories: [],
        status: null,
        query: []
    },
    reducers: {
        selectCategories: (state,action) => {
            state.query = action.payload
        },
    },
    extraReducers: {
        [getCategories.fulfilled]: (state, { payload }) => {
            state.categories = payload
            state.status = 'success'
        }, 
        [getCategories.rejected]: (state,action) => {
            state.status = 'failed'
        },
    }
});


const { reducer, actions } = category;
export const { selectCategories } = actions
export default reducer