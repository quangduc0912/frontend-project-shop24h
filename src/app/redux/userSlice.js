import { createSlice } from "@reduxjs/toolkit";

const user = createSlice({
    name: 'user',
    initialState: {
        user: ""
    },
    reducers: {
        loggedInUser: (state,action) => {
            state.user = action.payload
        }
    }
});

const { reducer, actions } = user;
export const {
  loggedInUser
} = actions;
export default reducer;