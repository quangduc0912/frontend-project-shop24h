import { createSlice } from "@reduxjs/toolkit";

const order = createSlice({
    name: 'order',
    initialState: {
        order: ""
    },
    reducers: {
        orderSuccess: (state,action) => {
            state.order = action.payload
        }
    }
});

const { reducer, actions } = order;
export const {
    orderSuccess
} = actions;
export default reducer;