import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios"; 

export const getPlatforms = createAsyncThunk('platform/getPlatforms', async() => {
    try {
        const response = await axios.get("https://backend-ecommerce-for-gamers.herokuapp.com/platforms");
        return response.data
    }
    catch(err) {
        return err.message
    }
})


const platform = createSlice({
    name: 'platform',
    initialState: {
        platforms: [],
        status: null,
        query: []
    },
    reducers: {
        selectPlatforms: (state,action) => {
            state.query = action.payload
        },
    },
    extraReducers: {
        [getPlatforms.fulfilled]: (state, { payload }) => {
            state.platforms = payload
            state.status = 'success'
        }, 
        [getPlatforms.rejected]: (state,action) => {
            state.status = 'failed'
        },
    }
});


const { reducer, actions } = platform;
export const { selectPlatforms } = actions
export default reducer