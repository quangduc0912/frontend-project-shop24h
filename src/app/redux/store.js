import { configureStore } from "@reduxjs/toolkit";
import categoryReducer from "./categorySlice";
import platformReducer from "./platformSlice";
import cartReducer from "./cartSlice";
import userReducer from "./userSlice";
import orderReducer from "./orderSlice"

const rootReducer = {
    category: categoryReducer,
    platform: platformReducer,
    cart: cartReducer,
    user: userReducer,
    order: orderReducer
}

const store = configureStore({
    reducer: rootReducer
})

export default store;