import { createSlice } from "@reduxjs/toolkit";
import { toast, Zoom } from "react-toastify";

const INITIAL_STATE = {
  cartItems: localStorage.getItem("cart")
    ? JSON.parse(localStorage.getItem("cart"))
    : [],
  cartTotal: 0,
  cartCheckOut: [],
};

const cart = createSlice({
  name: "cart",
  initialState: INITIAL_STATE,
  reducers: {
    addToCart: (state, action) => {
      const itemIndex = state.cartItems.findIndex(
        (item) => item._id === action.payload._id
      );

      if (itemIndex >= 0) {
        state.cartItems[itemIndex].amount += 1;
        state.cartItems[itemIndex].selected = false;
        localStorage.setItem("cart", JSON.stringify(state.cartItems));
      } else {
        const product = { ...action.payload, amount: 1, selected: false };
        state.cartItems.push(product);
        localStorage.setItem("cart", JSON.stringify(state.cartItems));
      }

      toast.success(`${action.payload.name} has been added to cart!`, {
        position: "top-center",
        autoClose: 1000,
        hideProgressBar: true,
      });
    },

    addToCartSinglePage: (state, action) => {
      const itemIndex = state.cartItems.findIndex(
        (item) => item._id === action.payload._id
      );

      if (itemIndex >= 0) {
        state.cartItems[itemIndex].amount =
          state.cartItems[itemIndex].amount + action.payload.quantity;
        state.cartItems[itemIndex].selected = false;
        localStorage.setItem("cart", JSON.stringify(state.cartItems));
      } else {
        const product = {
          ...action.payload,
          amount: action.payload.quantity,
          selected: false,
        };
        state.cartItems.push(product);
        localStorage.setItem("cart", JSON.stringify(state.cartItems));
      }

      toast.success(`${action.payload.name} has been added to cart!`, {
        position: "top-center",
        autoClose: 1000,
        hideProgressBar: true,
      });
    },

    buyNowCart: (state, action) => {
      const itemIndex = state.cartCheckOut.findIndex(
        (item) => item._id === action.payload._id
      );

      if (itemIndex >= 0) {
        state.cartCheckOut[itemIndex].amount =
          state.cartCheckOut[itemIndex].amount + action.payload.quantity;
        state.cartCheckOut[itemIndex].selected = true;
        state.cartItems = state.cartCheckOut;
      } else {
        const product = {
          ...action.payload,
          amount: action.payload.quantity,
          selected: true,
        };
        state.cartCheckOut.push(product);
        state.cartItems.push(product);
        localStorage.setItem("cart", JSON.stringify(state.cartItems));
      }

      state.cartTotal = state.cartCheckOut.reduce((acc, curr) => {
        return acc + (curr.buyPrice - curr.promotionPrice) * curr.amount;
      }, 0);

      toast.success(`${action.payload.name} is ready to checkout!`, {
        position: "top-center",
        autoClose: 1000,
        hideProgressBar: true,
      });
    },

    removeFromCart: (state, action) => {
      const updateCart = state.cartItems.filter(
        (item) => item._id !== action.payload._id
      );
      state.cartItems = updateCart;
      toast.error(`${action.payload.name} has been removed from cart!`, {
        position: "top-center",
        autoClose: 500,
        transition: Zoom,
      });
      localStorage.setItem("cart", JSON.stringify(state.cartItems));

      const updateTotal = state.cartItems.reduce((acc, curr) => {
        return acc + (curr.buyPrice - curr.promotionPrice) * curr.amount;
      }, 0);
      state.cartTotal = updateTotal;
    },

    clearCart: (state, action) => {
      state.cartItems = [];
      state.cartCheckOut = [];
      toast.error(`All products has been removed from cart!`, {
        position: "top-center",
        autoClose: 500,
        transition: Zoom,
      });
      state.cartTotal = 0;
      localStorage.setItem("cart", JSON.stringify(state.cartItems));
    },

    selectToCheckOut: (state, action) => {
      const cartSelected = state.cartItems.find(
        (item) => item._id === action.payload._id
      );
      cartSelected.selected = action.payload.checked;
      if (cartSelected.selected) {
        state.cartCheckOut.push(cartSelected);
        state.cartTotal = state.cartCheckOut.reduce((acc, curr) => {
          return acc + (curr.buyPrice - curr.promotionPrice) * curr.amount;
        }, 0);

        toast.success(`${action.payload.name} has been selected from cart!`, {
          position: "top-center",
          autoClose: 500,
          transition: Zoom,
          hideProgressBar: true,
        });
      } else {
        state.cartCheckOut = state.cartCheckOut.filter(
          (cart) => cart._id !== action.payload._id
        );
        toast.warn(`${action.payload.name} has been removed from cart!`, {
          position: "top-center",
          autoClose: 500,
          transition: Zoom,
          hideProgressBar: true,
        });
        state.cartTotal = state.cartCheckOut.reduce((acc, curr) => {
          return acc + (curr.buyPrice - curr.promotionPrice) * curr.amount;
        }, 0);
      }
    },

    selectAllToCheckOut: (state, action) => {
      state.cartItems.map((cart) => (cart.selected = action.payload.checked));

      if (action.payload.checked) {
        state.cartCheckOut = [...state.cartItems];
        state.cartTotal = state.cartCheckOut.reduce((acc, curr) => {
          return acc + (curr.buyPrice - curr.promotionPrice) * curr.amount;
        }, 0);

        toast.success(`All items has been selected from cart!`, {
          position: "top-center",
          autoClose: 500,
          transition: Zoom,
          hideProgressBar: true,
        });
      } else {
        toast.warn(`All items has been removed from cart!`, {
          position: "top-center",
          autoClose: 500,
          transition: Zoom,
          hideProgressBar: true,
        });
        state.cartCheckOut = [];
        state.cartTotal = 0;
      }
    },

    processToCheckOut: (state, action) => {
      const updateCart = state.cartItems.filter(
        (item) => item.selected !== action.payload.selected
      );
      state.cartItems = updateCart;
      localStorage.setItem("cart", JSON.stringify(state.cartItems));
    },

    incrementQuantity: (state, action) => {
      const itemIndex = state.cartItems.findIndex(
        (item) => item._id === action.payload._id
      );

      if (itemIndex >= 0) {
        state.cartItems[itemIndex].amount += 1;
        toast.success(`${action.payload.amount} has been updated to cart!`, {
          position: "top-center",
          autoClose: 1000,
          hideProgressBar: true,
        });
        state.cartItems[itemIndex].selected = false;
        localStorage.setItem("cart", JSON.stringify(state.cartItems));
      }
    },

    decrementQuantity: (state, action) => {
      const itemIndex = state.cartItems.findIndex(
        (item) => item._id === action.payload._id
      );

      if (itemIndex >= 0) {
        if (state.cartItems[itemIndex].amount > 1) {
          state.cartItems[itemIndex].amount -= 1;
          toast.success(
            `${action.payload.name} has been updated quantity cart!`,
            {
              position: "top-center",
              autoClose: 1000,
              hideProgressBar: true,
            }
          );
          state.cartItems[itemIndex].selected = false;
          localStorage.setItem("cart", JSON.stringify(state.cartItems));
        }
      }
    },

    clearCartCheckOut: (state, action) => {
      state.cartCheckOut = [];
      state.cartTotal = 0;
    },
  },
});

const { reducer, actions } = cart;
export const {
  addToCart,
  addToCartSinglePage,
  buyNowCart,
  removeFromCart,
  clearCart,
  selectToCheckOut,
  selectAllToCheckOut,
  incrementQuantity,
  decrementQuantity,
  processToCheckOut,
  clearCartCheckOut
} = actions;
export default reducer;
