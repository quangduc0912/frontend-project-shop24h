import firebase from "firebase";
import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyCS4s0D9qcefiOsYZ8ucnAbiBGHwQxbfKg",
    authDomain: "shop24h-devcamp.firebaseapp.com",
    projectId: "shop24h-devcamp",
    storageBucket: "shop24h-devcamp.appspot.com",
    messagingSenderId: "354029096619",
    appId: "1:354029096619:web:f47f1791b8a3778dee31ee"
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();

export const googleProvider = new firebase.auth.GoogleAuthProvider()
