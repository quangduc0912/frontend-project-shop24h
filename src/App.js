import "bootstrap/dist/css/bootstrap.min.css";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";

import { Routes, Route } from "react-router-dom";
import HomePage from "./views/pages/HomePage";
import ProductPage from "./views/pages/ProductPage";
import ProductInfoPage from "./views/pages/ProductInfoPage";
import React from "react";
import CartPage from "./views/pages/CartPage";
import { ToastContainer } from "react-toastify";
import ErrorPage from "./views/pages/ErrorPage";
import CheckOutPage from "./views/pages/CheckOutPage";
import SignUpPage from "./views/pages/SignUpPage";
import MyProfilePage from "./views/pages/MyProfilePage";
import OrderSuccessPage from "./views/pages/OrderSuccessPage";
import PlaystationPage from "./views/pages/Platforms/PlaystationPage";
import VideoGamePage from "./views/pages/Categories/VideoGame";
import AccessorizesPage from "./views/pages/Categories/Accessorizes";
import ConsolePage from "./views/pages/Categories/Console";
import XboxPage from "./views/pages/Platforms/XboxPage";
import NintendoPage from "./views/pages/Platforms/NintendoPage";
import PcPage from "./views/pages/Platforms/PcPage";


function App() {
  return (
    <div>
      <ToastContainer
      />
      <Routes>
        <Route path="*" element={<ErrorPage />} />
        <Route exact path="/" element={<HomePage />}></Route>
        <Route path="/products" element={<ProductPage />}></Route>
        <Route path="/cart" element={<CartPage />} />
        <Route
          path="/products/:productId"
          element={<ProductInfoPage />}
        />
        <Route path="/videogames" element={<VideoGamePage/>} />
        <Route path="/console" element={<ConsolePage/>} />
        <Route path="/accessorizes" element={<AccessorizesPage/>} />
        <Route path="/playstation" element={<PlaystationPage/>} />
        <Route path="/xbox" element={<XboxPage/>} />
        <Route path="/nintendo" element={<NintendoPage/>} />
        <Route path="/pc" element={<PcPage/>} />
        <Route path="/cart/checkout" element={<CheckOutPage/>} />
        <Route path="/order" element={<OrderSuccessPage/>} />
        <Route path="/signup" element={<SignUpPage/>} />
        <Route path="/myspace" element={<MyProfilePage/>} />
      </Routes>
    </div>
  );
}

export default App;
